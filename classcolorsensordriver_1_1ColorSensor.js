var classcolorsensordriver_1_1ColorSensor =
[
    [ "__init__", "classcolorsensordriver_1_1ColorSensor.html#a56cf8f7931ac62023162e6e003eee881", null ],
    [ "disable", "classcolorsensordriver_1_1ColorSensor.html#a00ffd0b10b22f5afa081708623b24c0d", null ],
    [ "enable", "classcolorsensordriver_1_1ColorSensor.html#a3669ff08a87d7d5581255aafad21a4e4", null ],
    [ "get_RGBdata", "classcolorsensordriver_1_1ColorSensor.html#a043707f83745a1b41b27af4b4203fbe3", null ],
    [ "read_byte", "classcolorsensordriver_1_1ColorSensor.html#afb8cf5566281867f52df10de83c2db90", null ],
    [ "read_word", "classcolorsensordriver_1_1ColorSensor.html#a66092ca53746060e97af48ab992d54c3", null ],
    [ "remap", "classcolorsensordriver_1_1ColorSensor.html#abc29becf6d65928c0e0b470661053a3c", null ],
    [ "write_byte", "classcolorsensordriver_1_1ColorSensor.html#a43411eccdfd7990ebbefa50a0c030909", null ],
    [ "address", "classcolorsensordriver_1_1ColorSensor.html#a4c7d91b865028822b417c2717b4f0035", null ],
    [ "debug", "classcolorsensordriver_1_1ColorSensor.html#a2f695da60503c03b50caae305b6d0858", null ],
    [ "i2c", "classcolorsensordriver_1_1ColorSensor.html#a0fb64879973b4894623c617c8d05ab9a", null ]
];